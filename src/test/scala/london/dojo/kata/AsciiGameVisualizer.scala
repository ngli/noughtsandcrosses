package london.dojo.kata

import london.dojo.kata.NoughtsAndCrosses.Square

class AsciiGameVisualizer(game: NoughtsAndCrosses) {

  val outerHorzBorder =".===.===.===.\r\n"
  val midHorzBorder ="|===|===|===|\r\n"

  def show: String = {
    (outerHorzBorder +
    (0 to 2).map(row).mkString(midHorzBorder) +
    outerHorzBorder).trim
  }

  def row(n:Int) = {
    "|" +game.board(n).map(s => " " + sq(s) + " ").mkString("|") + "|\r\n"
  }

  def sq(square: Square):String = square match {
    case None => " "
    case Some(tk) => tk.toString
  }
}
