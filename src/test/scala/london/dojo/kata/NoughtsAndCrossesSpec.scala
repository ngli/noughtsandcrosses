package london.dojo.kata

import london.dojo.kata.NoughtsAndCrosses._
import org.scalatest.{DiagrammedAssertions, FlatSpec, Matchers}

class NoughtsAndCrossesSpec() extends FlatSpec with Matchers with DiagrammedAssertions {

  "a game" should "start without a victor" in {

    val game = newGame()
    assert(!game.gameOver)
    assert(game.victor.isEmpty)
  }

  it should "player1 starts first" in {
    val game = newGame()
    assert(game.currentTurn == game.player1)
  }

  "players" should "each have a different O/X token" in {
    val game = newGame()
    List(game.player1.token, game.player2.token) should contain allOf(O, X)
    assert(game.player1.token != game.player2.token)
  }

  "The board" should "initially be empty" in {
    val game = newGame()
    assert(game.board.forall(_.forall(_.isEmpty)))
  }

  "The board" can "be visualised" in {
    val game = newGame()
    assert(visualize(game) ==
      """.===.===.===.
        #|   |   |   |
        #|===|===|===|
        #|   |   |   |
        #|===|===|===|
        #|   |   |   |
        #.===.===.===.""".stripMargin('#'))
  }

  "A token" can "be placed on the board" in {
    val game1 = newGame()
    val game2: NoughtsAndCrosses = game1.applyMove(1, 1)
    assert(visualize(game2) ==
      """#.===.===.===.
        #|   |   |   |
        #|===|===|===|
        #|   | O |   |
        #|===|===|===|
        #|   |   |   |
        #.===.===.===.""".stripMargin('#'))
  }

  "The next turn" should "be different after a move is made" in {
    val game1 = newGame()
    val firstPlayersTurn = game1.currentTurn
    val game2: NoughtsAndCrosses = game1.applyMove(1, 1)
    game2.currentTurn should not be firstPlayersTurn

    val game3: NoughtsAndCrosses = game2.applyMove(1, 2)
    game3.currentTurn shouldBe firstPlayersTurn
  }

  "The correct token" should "be used for each player" in {
    val game1 = newGame()
    val game2 = game1.applyMove(1, 0)
    val game3 = game2.applyMove(2, 2)
    assert(visualize(game2) ==
      """#.===.===.===.
        #|   | O |   |
        #|===|===|===|
        #|   |   |   |
        #|===|===|===|
        #|   |   |   |
        #.===.===.===.""".stripMargin('#'))
    assert(visualize(game3) ==
      """#.===.===.===.
        #|   | O |   |
        #|===|===|===|
        #|   |   |   |
        #|===|===|===|
        #|   |   | X |
        #.===.===.===.""".stripMargin('#'))
  }

  "Invalid moves" should "be detected" in {
    val game1 = newGame().applyMove(1, 1)
    game1.isAvailableSquare(1, 1) shouldBe false
    game1.isAvailableSquare(2, 0) shouldBe true
  }

  "Applying a second move to a square" should "not be allowed" in {
    val game1 = newGame().applyMove(1, 1)
    intercept[IllegalArgumentException] {
      val game2 = game1.applyMove(1, 1)
    }
    intercept[IllegalArgumentException] {
      val game3 = game1.applyMove(3, 1)
    }
    intercept[IllegalArgumentException] {
      val game4 = game1.applyMove(1, 3)
    }
  }

  "A winning row move" should "end the game" in {
    val (player1, player2) = (Player("1", O), Player("2", X))
    val game1 = newGame(player1, player2)
    val endedGame = game1.
      applyMove(0, 0). // P1
      applyMove(0, 1). // P2
      applyMove(1, 0). // P1
      applyMove(0, 2). // P2
      applyMove(2, 0) // P1
    /* assert(visualize(endedGame) ==
       """#.===.===.===.
         #| O | O | O |
         #|===|===|===|
         #| X |   |   |
         #|===|===|===|
         #| X |   |   |
         #.===.===.===.""".stripMargin('#'))*/
    assert(endedGame.victor == Some(player1))
  }

  "A won game " should "not be playable" in {
    val (player1, player2) = (Player("1", O), Player("2", X))

    val wonGame = NoughtsAndCrosses(
      board = List(
        List(Some(O), Some(O), Some(O)),
        List(Some(X), None, None),
        List(Some(X), None, None)),
      player1 = player1, player2 = player2,
      currentTurn = player2)

    assert(wonGame.gameOver)
    the[IllegalStateException] thrownBy {
      wonGame.applyMove(1, 1)
    } should have message "Cannot continue - player 1 has just won!"
  }


  "A winning column move" should "end the game" in {
    val (player1, player2) = (Player("1", O), Player("2", X))
    val game1 = newGame(player1, player2)
    val endedGame = game1.
      applyMove(0, 0). // P1
      applyMove(1, 0). // P2
      applyMove(0, 1). // P1
      applyMove(2, 0). // P2
      applyMove(0, 2) // P1
    /*assert(visualize(endedGame) ==
      """#.===.===.===.
        #| O | X | X |
        #|===|===|===|
        #| O |   |   |
        #|===|===|===|
        #| O |   |   |
        #.===.===.===.""".stripMargin('#'))*/
    assert(endedGame.victor == Some(player1))
  }

  "A winning diagonal move" should "end the game" in {
    val (player1, player2) = (Player("1", O), Player("2", X))
    val diagonalRightwonGame = NoughtsAndCrosses(
      board = List(
        List(Some(O), Some(X), None),
        List(Some(X), Some(O), None),
        List(None, None, Some(O))),
      player1 = player1, player2 = player2,
      currentTurn = player2)
    assert(diagonalRightwonGame.victor == Some(player1))
    val diagonalLeftwonGame = NoughtsAndCrosses(
      board = List(
        List(None, Some(X), Some(O)),
        List(Some(X), Some(O), None),
        List(Some(O), None, None)),
      player1 = player1, player2 = player2,
      currentTurn = player2)
    assert(diagonalLeftwonGame.victor == Some(player1))
  }

  def newGame(player1: Player = Player("1", O), player2: Player = Player("2", X)): NoughtsAndCrosses = {
    new NoughtsAndCrosses(List.fill(3)(List.fill(3)(None)), player1, player2, player1)
  }

  def visualize(game: NoughtsAndCrosses) = {
    new AsciiGameVisualizer(game).show
  }
}