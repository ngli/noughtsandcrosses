package london.dojo.kata

import london.dojo.kata.NoughtsAndCrosses.{X, O, Player}

import scala.io.StdIn

object NoughtsAndCrossesConsoleGame extends App{

  println("Welcome, enter player 1's name:")
  val player1 = Player(StdIn.readLine(), O)
  printPlayerInfo(player1)
  println("Welcome, enter player 2's name:")
  val player2 = Player(StdIn.readLine(), X)
  printPlayerInfo(player2)

  def printPlayerInfo(player: Player): Unit = {
    println(s"${player.name} is ${player.token}")
  }


}
