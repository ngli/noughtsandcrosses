package london.dojo.kata

import NoughtsAndCrosses._

case class NoughtsAndCrosses(board: List[List[Square]], player1: Player, player2: Player, currentTurn: Player) {

  def applyMove(x: Int, y: Int): NoughtsAndCrosses = {
    victor.foreach { player=>
      throw new IllegalStateException(s"Cannot continue - player ${player.name} has just won!")
    }
    if (isAvailableSquare(x, y)) {
      val newBoard: List[List[Square]] = board.updated(y, board(y).updated(x, Some(currentTurn.token)))
      this.copy(board = newBoard, currentTurn = nextTurn)
    } else throw new IllegalArgumentException("Invalid move")
  }

  def justPlayedPlayer = nextTurn
  def nextTurn = if (currentTurn == player1) player2 else player1

  def victor:Option[Player] = if(boardHasStreakOf(justPlayedPlayer.token)) Some(justPlayedPlayer) else None

  private def boardHasStreakOf(token: Token) = {
    val lastIndex = board.size -1
    val hasRowStreak = board.exists(_.forall(_.contains(token)))
    val winningColumn = board.indices.exists(c => board.forall(r =>r(c).contains(token)))
    val winningDiagonal =
      board.indices.forall(d => board(d)(d).contains(token)) ||
        board.indices.forall(d => board(d)(lastIndex-d).contains(token))
    hasRowStreak||winningColumn||winningDiagonal
  }

  def gameOver = victor.isDefined

  def isAvailableSquare(x: Int, y: Int): Boolean = {
    y >= 0 &&
      y < board.length &&
      x >= 0 &&
      x < board(0).length &&
      board(y)(x).isEmpty
  }




}
object NoughtsAndCrosses {
  sealed trait Token {}
  case object X extends Token
  case object O extends Token

  type Square = Option[Token]

  case class Player(name: String, token:Token)

}